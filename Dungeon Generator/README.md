# Dungeon Generator

An algorithm that allows you to generate 3D dungeons in Unity3D.


## Getting Started

The classes contained in the repositories allow to generate dungeons and provide all a series of options to determine the structural characteristics.

### Prerequisites

The algorithm is written in C# in particular the standard version of .NET 2.0 has been used.

Unity 2019.3.9.f1 has been used for development but the code *should* work on all versions of Unity that support this version of .NET.

### Installing

To use the algorthm, follow the steps below:

* Create a new project in Unity3D or use an existing project.
* Copy the files "DungeonGenerator.cs" and "Tale.cs" in the project assets folder.
* Add the "DungeonGenerator" component to an object in your scene.
* Enter all the parameters of the dungeon.
* Start the simulation or use the button in the "Generate Dungeon" inspector at runtime to generate a new dungeon in the scene.


## Usage

Once the rule has been added to any object, the values must be entered in the inspector to generate the desired dungeon.

We will first have to enter the general values of our dungeon as:

* Posion of the dungeon
* Rotation of the dungeon
* Scaling of the dungeon

**Attention:** Keep in mind when entering data that the dungeon pivot is located in the **upper left corner** of the generated structure.

Then the size of the dungeon must be entered.

Afterwards, through a particular check, we could decide whether to generate random dungeons or whether to use a seed if the tick was not present.

Then it will be possible to select the density of the rooms using the appropriate slider, how much of the dungeon surface will be occupied by rooms and by corridors.
(0 there are only corridors, 0.5 if the dungeon is half formed by rooms)

Finally, it will be up to you to choose whether to use prefabs for the construction of the dungeon or to leave the task to the generator.
In case you decide to let the algorithm  generate the dungeon, then you will be asked to enter the materials for walls, floor and ceiling and finally the height of the dungeon.
Otherwise the user will be asked to insert a series of prefabs that will be used for the creation of the dungeon and then insert all the measures concerning the size of the prefabs and the thickness of the walls.

Finally the last check box will be used to allow the user to choose whether to delete the dungeons present in the scene when clicking 
on the "Generate Dungeon" button or simply to recreate a new one without touching the previously created dungeons. **Attention: To delete the dungeons present in the scene, the algorithm 
assigns to each dungeon created the "Finish" tag already present in Unity to make the software work stand alone. If you check the checkbox keep in mind that the algorithm could also delete other objects 
in the scene with the same "Finish" tag.**

## Prefab Construction
If you want to customize the dungeon, the algorithm  allows you to insert custom prefabs.
In order to work fine, the algorithm needs that the size of the prefabs be consistent and that all the measurements required by the algorithm are reported as accurately as possible.
The prefabs that the algorithm needs are:

* Impasse
* Curve
* Corridor
* T-Joint
* Cross Joint
* Wall with Door
* Simple Wall
* Floor
* Ceiling

The algorithm builds the dungeon using a set of tiles. They must be square in shape and all of the same size in order to adhere perfectly and create a unique surface.
Once you have established the size of a tile, the thickness of the walls and the height of a tile, you can proceed with the construction of the tiles.

First of all create the prefabs to use as a floor or ceiling. These can be trivially plans.
Make sure they are the size of a tile and their prefabs are centered.

Now create the prefab to use for building the walls.
To do this, create a cube as high as the height of the tile and the thickness you have established for the walls.
Make sure the pivot is centered.

To build corridors that will be used to connect the rooms, you must create a series of prefabs to build 
T-joints, Cross-Joints, curves and dead ends.

To do that fist you need to establish the corridor width (which obviously does not exceed the size of the single tile) and the tiles must be built making sure that the width of the corridors is always the same between the individual tiles.

The walls used to create these structures must have the height and thickness initially established.

See the image for more details on how to build these tiles.

![Tile Scheme](./Images/Tile-Scheme.png "Tile Scheme") 

The tiles must be oriented as shown in the figure.

![Tile view in 3D](./Images/Tile-View.png "Tile view in 3D")

Finally, it is possible to build the wall with the door that is used to connect the corridors to the rooms. The size of the door must be smaller than the width of the corridor to have a good result. Also in this case height, width and thickness of the walls must be consistent and the pivots centered.

![Final Result with Prefabs](./Images/Final-Result.png "Final Result with Prefabs")

For example, using low poly fantasy prefabs, a result like the one shown in the figure is possible.

## Built With

* [Unity](https://docs.unity3d.com/Manual/index.html) - The game engine.
* [C#](https://docs.microsoft.com/it-it/dotnet/csharp/) - The language used.


## Versioning

We use [BitBucket](https://bitbucket.org/) for versioning. 


## Authors

* **Marco Picariello** 
* **Sara Borriello** 