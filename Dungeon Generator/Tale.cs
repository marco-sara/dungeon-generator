﻿/*
 Authors: Marco Picariello, Sara Borriello
 Language: C#
 Description: This class is used into DungeonGenerator class for instantiate the tale of the dungeon.
*/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Tale {
    // These variables are used to indicates the openings and the type of a tale.

    //Direction indicies: [0] up, [1] right, [2] down , [3] left
    private bool[] direction;
    private bool isFutureRoom;
    //Taletype values: 0 cross, 1 impasse, 2 curve, 3 corridor, 4 t join 
    private int taletype;
    private int talerot;

    public Tale(bool[] direction, bool isFutureRoom) {
        this.direction = direction;
        this.isFutureRoom = isFutureRoom;
        DefineTale();
    }

    public Tale() {

    }

    //This method are called when we call the constructor of the class for determinate the type of tale and his rotation.
    public void DefineTale() {
        if (Enumerable.SequenceEqual(direction, new bool[] { false, false, false, false })) {
                Debug.LogError("Error: This tale should't exist");
            } else if (Enumerable.SequenceEqual(direction, new bool[] { false, false, false, true })) {
                taletype = 1; talerot = 180;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { false, false, true, false })) {
                taletype = 1; talerot = 90;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { false, false, true, true })) {
                taletype = 2; talerot = 0;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { false, true, false, false })) {
                taletype = 1; talerot = 0;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { false, true, false, true })) {
                taletype = 3; talerot = 0;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { false, true, true, false })) {
                taletype = 2; talerot = -90;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { false, true, true, true })) {
                taletype = 4; talerot = -90;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { true, false, false, false })) {
                taletype = 1; talerot = -90;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { true, false, false, true })) {
                taletype = 2; talerot = 90;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { true, false, true, false })) {
                taletype = 3; talerot = 90;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { true, false, true, true })) {
                taletype = 4; talerot = 0;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { true, true, false, false })) {
                taletype = 2; talerot = 180;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { true, true, false, true })) {
                taletype = 4; talerot = 90;
            } else if (Enumerable.SequenceEqual(direction, new bool[] { true, true, true, false })) {
                taletype = 4; talerot = 180;
            } else {
                taletype = 0; talerot = 0;
            }
    }

    public bool IsFutureRoom { get => isFutureRoom; set => isFutureRoom = value; }

    public int Taletype { get => taletype;}

    public int Talerot { get => talerot;}

    public bool[] Direction {
        get => direction;
        set {
            direction = value;
            DefineTale();
        }
    }
}